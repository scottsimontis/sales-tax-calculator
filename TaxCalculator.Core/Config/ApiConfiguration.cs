﻿namespace TaxCalculator.Core.Config {
    public class ApiConfiguration {
        public static string SectionKey = "ApiRegistrations";

        public string Name { get; }

        public string BaseAddress { get; }
    }
}
