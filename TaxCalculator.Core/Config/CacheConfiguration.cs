﻿namespace TaxCalculator.Core.Config {
    public class CacheConfiguration {
        public static string SectionKey = "Caching";

        public int AbsoluteExpirationSeconds { get; }

        public int SlidingWindowExpirationSeconds { get; }
    }
}
