﻿using System;
using System.Collections.Generic;
using TaxCalculator.Core.Models;
using TaxCalculator.Core.Validation;

namespace TaxCalculator.Core.Functors {
    public static class Either {
        public static Either<TL, TR> Right<TL, TR>(TR right) => new Either<TL, TR>(right);

        public static Either<TL, TR> Left<TL, TR>(TL left) => new Either<TL, TR>(left);
    }

    public static class EitherExtensions {
        public static void Match<TL, TR>(this Either<TL, TR> input, Action<TR> right, Action<TL> left,
            Action bottom = null) {
            if (input.ThrowIfNull(nameof(input)).State == EitherState.Right) {
                right.ThrowIfNull(nameof(right))(input.Right);
            }
            else if (input.State == EitherState.Left) {
                left.ThrowIfNull(nameof(left))(input.Left);
            }
            else {
                bottom?.Invoke();
            }
        }

        public static List<TR> ToList<TL, TR>(this Either<TL, TR> input) =>
            input.ThrowIfNull(nameof(input)).IsRight switch {
                true => new List<TR>() {input.Right},
                _ => new List<TR>()
            };

        public static TR[] ToArray<TL, TR>(this Either<TL, TR> input) =>
            input.ThrowIfNull(nameof(input)).IsRight switch {
                true => new[] {input.Right},
                _ => new TR[0]
            };

        public static int Count<TL, TR>(this Either<TL, TR> input) => input.ThrowIfNull(nameof(input)).IsRight ? 1 : 0;

        public static Either<TR, TL> Swap<TL, TR>(this Either<TL, TR> input) =>
            input.ThrowIfNull(nameof(input)).IsRight
                ? new Either<TR, TL>(input.Right)
                : input.IsLeft
                    ? new Either<TR, TL>(input.Left)
                    : new Either<TR, TL>();
    }
}
