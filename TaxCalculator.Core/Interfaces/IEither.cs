﻿using System;

namespace TaxCalculator.Core.Interfaces {
    public interface IEither {
        bool IsRight { get; }
        bool IsLeft { get; }

        TResult MatchUntyped<TResult>(Func<object, TResult> right, Func<object, TResult> left);

        Type GetUnderlyingLeftType();

        Type GetUnderlyingRightType();
    }
}
