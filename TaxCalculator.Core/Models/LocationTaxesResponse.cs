﻿using System.Collections.Generic;

namespace TaxCalculator.Core.Models {
    public abstract class LocationTaxesResponse {
        public string Country { get; }

        public string PostalCode { get; }

        public IEnumerable<TaxRate> Rates { get; }

        protected LocationTaxesResponse(string postalCode, string country, IEnumerable<TaxRate> rates) {
            Country = country;
            PostalCode = postalCode;
            Rates = rates;
        }
    }
}
