﻿namespace TaxCalculator.Core.Models {
    public class CalculationRequest {
        public Address From { get; }

        public Address To { get; }

        public decimal OrderPrice { get; }

        public decimal ShippingPrice { get; }

        public CalculationRequest(Address from, Address to, decimal orderPrice, decimal shippingPrice) {
            From = from;
            To = to;
            OrderPrice = orderPrice;
            ShippingPrice = shippingPrice;
        }
    }
}
