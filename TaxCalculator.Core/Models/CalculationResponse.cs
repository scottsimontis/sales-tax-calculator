﻿namespace TaxCalculator.Core.Models {
    public class CalculationResponse {
        public decimal OrderTotal { get; }

        public decimal TaxRate { get; }

        public decimal TaxesCollected { get; }
    }
}
