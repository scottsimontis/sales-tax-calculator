﻿namespace TaxCalculator.Core.Models {
    public class TaxRate {
        public string Description { get; }

        public decimal Rate { get; }

        public TaxRate(string description, decimal rate) {
            Description = description;
            Rate = rate;
        }
    }
}
