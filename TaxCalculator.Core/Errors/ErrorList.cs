﻿using System;
using System.Collections.Generic;
using System.Net;
using TaxCalculator.Core.Validation;

namespace TaxCalculator.Core.Errors {
    public class ErrorList : List<Error> {
        public readonly DateTimeOffset UtcTimestamp = DateTimeOffset.UtcNow;

        public readonly Uri Location;

        public readonly HttpStatusCode StatusCode;

        public ErrorList(Uri location, HttpStatusCode statusCode) {
            if (!Enum.IsDefined(typeof(HttpStatusCode), statusCode)) {
                throw new InvalidOperationException($"Cannot represent HTTP status code with value {statusCode} as a known HTTP status code.");
            }

            StatusCode = statusCode;
            Location = location.ThrowIfNull(nameof(location));
        }
    }
}
