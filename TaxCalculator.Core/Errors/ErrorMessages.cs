﻿namespace TaxCalculator.Core.Errors {
    public static class ErrorMessages {
        public static string StringLengthMessage =
            "String {PropertyName} was expected to have length of {ExpectedLength} but had length {ActualLength}.";

        public static string StringNullOrEmptyMessage =
            "String {PropertyName} was expected to have a value, but was set to empty/null state {PropertyValue}.";

        public static string StringNotLetters =
            "String {PropertyName} was expected to contain mostly letters with limited whitespace and punctuation, not {PropertyValue}.";

        public static string PostalCodeNotAmerican =
            "String {PropertyName} currently has value {PropertyValue}, which is not a valid American postal code.";

        public static string StringMustHaveLettersOnlyStrict =
            "String {PropertyName} with value {PropertyValue} contains non-letter characters.";

        public static string PostalCodeNotEuropean =
            "String {PropertyName} has value {PropertyValue}, which is not a valid postal code for {Country}...expected {Pattern}";

        public static string PostalCodeNotAustralian =
            "String {PropertyName} has value {PropertyValue}, which is not a valid Australian postal code.";

        public static string PostalCodeNotCanadian =
            "String {PropertyName} has value {PropertyValue}, which is not a valid Canadian postal code.";

        public static string CurrencyMustBePositive =
            "Currency denomination {PropertyName} has value {PropertyValue}, which is not a valid (positive) amount of currency.";

        public static string CollectionMustBeNullOrEmpty =
            "Collection {PropertyName} was expected to be null or empty, but one or more values were discovered.";

        public static string CalculationRequestMustHaveAmountSet =
            "The calculation endpoint requires the request data to include a dollar amount for unit item aggregate cost; supply either all line items of order or specify final price of all items.";

        public static string CalculationRequestMustNotHaveBothAmountsSet =
            "Two amounts have been specified for this order; both order line items and final dollar amount were specified. Behavior is undefined for this case, so further execution was halted.";

        public static string CalculationLineItemsMustHavePositiveOrZeroCost =
            "The line item was expected to have a positive or zero cost; instead, the value {PropertyValue} was encountered.";

        public static string CalculationRequestExemptionNotFound =
            "Exemption type {PropertyValue} is not a valid exemption type for a calculation request.";

        public static string IntMustNotBeNegative =
            "The integer {PropertyName} was expected to be positive or zero, but instead has value {PropertyValue}.";

        public static string IntParsingFailed =
            "Failed to parse the string {PropertyValue} into a valid decimal value, please verify a valid string representation of a number was provided.";

        public static string IntMustBePositive =
            "The scalar quantity {PropertyName} was expected to be a positive number, but instead has value {PropertyValue}.";

        public static string DecimalMustNotBeNegative =
            "The decimal {PropertyName} was expected to be positive or zero, but instead has value {PropertyValue}.";

        public static string DecimalParsingFailed =
            "Failed to parse the string {PropertyValue} into a valid decimal value, please verify a valid string representation of a number was provided.";

        public static string DecimalMustBeLessThan =
            "The scalar quantity {PropertyName} was expected to be less than or equal to {MaxValue}, but instead has value {PropertyValue}.";

        public static string TaxJarUnsupportedCountry =
            "The TaxJar Tax API does not support the country {PropertyValue} at this time, please check the API documentation for a current list of supported countries.";

        public static string LineItemDiscountCannotExceedPrice =
            "The Discount value of {Discount} set on the current line item exceeds the unit price {Price} of the line item.";

        public static string ToStateNotValidIsoCode =
            "Addresses in Canada and America must supply a valid two-letter ISO code for the ToState value, found {PropertyValue} instead.";

        public static string FromStateNotValidIsoCode =
            "Addresses in Canada and America must supply a valid two-letter ISO code for the FromState value, found {PropertyValue} instead.";
    }
}
