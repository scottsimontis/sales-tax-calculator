﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using FluentValidation;
using TaxCalculator.Core.Errors;

namespace TaxCalculator.Core.Validation {
    public static class ValidatorExtensions {
        private static readonly string AustralianPostalCodePattern = "^\\d{4}$";

        private static readonly string AmericanPostalCodePattern = "(^\\d{5}-\\d{4})$|^(\\d{5})$";

        private static readonly string CanadianPostalCode = "[A-Z]\\d[A-Z] \\d[A-Z]\\d";

        public static IRuleBuilderOptions<T, string> MustHaveExactLengthOf<T>(this IRuleBuilder<T, string> builder,
            int length) =>
            builder.Must((root, str, context) => {
                context.MessageFormatter.AppendArgument("ExpectedLength", length);
                context.MessageFormatter.AppendArgument("ActualLength", str?.Length ?? 0);

                return (str?.Length ?? 0) == length;
            })
                .WithMessage(ErrorMessages.StringLengthMessage)
                .WithErrorCode(ErrorCodes.StringLengthError);

        public static bool IsAmericanPostCode(this string postalCode) {
            if (string.IsNullOrEmpty(postalCode)) {
                return false;
            }

            var americanPostalCode = new Regex(
                AmericanPostalCodePattern,
                RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.CultureInvariant);

            return americanPostalCode.IsMatch(postalCode);
        }

        public static bool IsAustralianPostCode(this string postalCode) {
            if (string.IsNullOrEmpty(postalCode)) {
                return false;
            }

            var australianPostCode = new Regex(
                AustralianPostalCodePattern,
                RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.CultureInvariant);

            return australianPostCode.IsMatch(postalCode);
        }

        public static bool IsCanadianPostCode(this string postalCode) {
            if (string.IsNullOrEmpty(postalCode)) {
                return false;
            }

            var canadianPostCode = new Regex(
                CanadianPostalCode,
                RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.CultureInvariant);

            return canadianPostCode.IsMatch(postalCode);
        }

        public static IRuleBuilderOptions<T, decimal> AmountIsPositive<T>(this IRuleBuilder<T, decimal> builder) =>
            builder.Must(x => x > 0)
                .WithMessage(ErrorMessages.CurrencyMustBePositive)
                .WithErrorCode(ErrorCodes.CurrencyNotPositiveAmount);

        public static IRuleBuilderOptions<T, int> AmountIsPositive<T>(this IRuleBuilder<T, int> builder) =>
            builder.Must(x => x > 0)
                .WithMessage(ErrorMessages.IntMustBePositive)
                .WithErrorCode(ErrorCodes.IntMustBePositive);

        public static IRuleBuilderOptions<T, IEnumerable<TData>> CollectionEmptyOrNull<T, TData>(
            this IRuleBuilder<T, IEnumerable<TData>> builder) =>
            builder.Must(x => x is null || !x.Any())
                .WithMessage(ErrorMessages.CollectionMustBeNullOrEmpty)
                .WithErrorCode(ErrorCodes.CollectionMustBeNullOrEmpty);

        public static IRuleBuilderOptions<T, IEnumerable<TData>> CollectionHasValues<T, TData>(
            this IRuleBuilder<T, IEnumerable<TData>> builder) =>
            builder.Must(x => x != null && x.Any())
                .WithMessage(ErrorMessages.CollectionMustBeNullOrEmpty)
                .WithErrorCode(ErrorCodes.CollectionMustBeNullOrEmpty);

        public static IRuleBuilderOptions<T, int> ScalarNotNegative<T>(this IRuleBuilder<T, int> builder) =>
            builder.Must(x => x >= 0)
                .WithMessage(ErrorMessages.IntMustNotBeNegative)
                .WithErrorCode(ErrorCodes.IntMustNotBeNegative);

        public static IRuleBuilderOptions<T, decimal> ScalarNotNegative<T>(this IRuleBuilder<T, decimal> builder) =>
            builder.Must(x => x >= 0M)
                .WithMessage(ErrorMessages.DecimalMustNotBeNegative)
                .WithErrorCode(ErrorCodes.DecimalMustNotBeNegative);

        public static IRuleBuilderOptions<T, string> MustHaveValue<T>(this IRuleBuilder<T, string> builder) =>
            builder.Must(x => !string.IsNullOrEmpty(x))
                .WithMessage(ErrorMessages.StringNullOrEmptyMessage)
                .WithErrorCode(ErrorMessages.StringNullOrEmptyMessage);

        public static IRuleBuilderOptions<T, string> MustBeComposedOfLetters<T>(
            this IRuleBuilder<T, string> builder) =>
            builder.Must((root, str, context) => {
                    if (string.IsNullOrEmpty(str)) {
                        return true;
                    }

                    return str.ToCharArray().All(
                        c =>
                            char.IsLetter(c) ||
                            char.IsWhiteSpace(c) ||
                            c == '.' ||
                            c == ',' ||
                            c == '-' ||
                            c == '\'');
                })
                .WithMessage(ErrorMessages.StringNotLetters)
                .WithErrorCode(ErrorCodes.StringNotLetters);

        public static IRuleBuilderOptions<T, string> MustHaveLettersOnly<T>(this IRuleBuilder<T, string> builder) =>
            builder.Must((root, str, context) =>
                string.IsNullOrEmpty(str) || str.ToCharArray().All(char.IsLetter))
                .WithErrorCode(ErrorCodes.StringMustHaveLettersOnlyStrict)
                .WithMessage(ErrorMessages.StringMustHaveLettersOnlyStrict);
    }
}
