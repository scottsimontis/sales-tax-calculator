﻿using System;

namespace TaxCalculator.Core.Validation {
    public static class ValidationExtensions {
        public static T ThrowIfNull<T>(this T value, string argName = "") =>
            value ?? throw new ArgumentNullException(string.IsNullOrEmpty(argName) ? nameof(value) : argName,
                "Expected a reference value, but null value was encountered instead.");
    }
}
