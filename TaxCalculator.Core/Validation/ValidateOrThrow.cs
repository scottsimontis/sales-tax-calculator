﻿using System;

namespace TaxCalculator.Core.Validation {
    public static class ValidateOrThrow {
        public static void NotNull<T>(T value, string argName = "") {
            if (string.IsNullOrEmpty(argName)) {
                argName = nameof(value);
            }

            if (value is null) {
                throw new ArgumentNullException(argName, "Value was expected to be a non-null reference; encountered null value.");
            }
        }

        public static void NotEmpty(string value, string argName = "") {
            if (string.IsNullOrEmpty(argName)) {
                argName = nameof(value);
            }

            if (string.IsNullOrEmpty(value)) {
                throw new ArgumentException("Value is required, but a null reference or empty string was encountered.", argName);
            }
        }
    }
}
