﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.Core.Interfaces;
using TaxCalculator.Core.Models;
using TaxCalculator.Core.Validation;

namespace TaxCalculator.Core.Services {
    public abstract class TaxService : ITaxService {
        private readonly ITaxCalculator _calculator;

        public TaxService(ITaxCalculator calculator) {
            _calculator = calculator;
        }

        public Either<ErrorList, CalculationResponse> CalculateTaxesOnOrder(CalculationRequest request) =>
            _calculator.CalculateTaxesOnOrder(request.ThrowIfNull(nameof(request)));

        public Either<ErrorList, LocationTaxesResponse> GetTaxesForLocation(LocationTaxesRequest request) =>
            _calculator.GetTaxesForLocation(request.ThrowIfNull(nameof(request)));
    }
}
