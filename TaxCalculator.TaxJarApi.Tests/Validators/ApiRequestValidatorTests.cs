﻿using Bogus;
using FluentValidation.TestHelper;
using TaxCalculator.Core.Models;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;
using TaxCalculator.TaxJarApi.Validators.Request.Calculation;
using Xunit;

namespace TaxCalculator.TaxJarApi.Tests.Validators {
    public class ApiRequestValidatorTests {
        private readonly ApiRequestValidator _sut;

        private readonly Faker _faker;

        public ApiRequestValidatorTests() {
            _sut = new ApiRequestValidator(new LineItemValidator(), new NexusAddressValidator());
            _faker = new Faker();
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_IfAmountZeroAndNoLineItems() {
            var model = new ApiCalculationRequest {
                                                      Amount = default
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_IfAmountNonZeroAndLineItemsPresent() {
            var model = new ApiCalculationRequest {
                                                      Amount = 5M
                                                  };
            model.LineItems.Add(new LineItem() {
                                                   Id = "123",
                                                   NonUnitItemDiscount = 0M,
                                                   ProductTaxCode = string.Empty,
                                                   Quantity = 4,
                                                   UnitPrice = 1M
                                               });

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x);
        }

        [Fact]
        public void ApiRequestValidator_Okay_IfAmountPositiveAndNoLineItemsPresent() {
            var model = new ApiCalculationRequest {
                                                      Amount = 5M
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x);
        }

        [Fact]
        public void ApiRequestValidator_Okay_IfAmountNotSetAndLineItemsPresent() {
            var model = new ApiCalculationRequest();
            model.LineItems.Add(new LineItem() {
                                                   Id = "123",
                                                   NonUnitItemDiscount = 0M,
                                                   ProductTaxCode = string.Empty,
                                                   Quantity = 4,
                                                   UnitPrice = 1M
                                               });

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_WhenToCountryCodeIsNull() {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = null
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToCountryCode);
        }

        [Theory]
        [InlineData("")]
        [InlineData("a")]
        [InlineData("abc")]
        [InlineData("!3.")]
        [InlineData("floccinaucinihilipilification")]
        public void ApiRequestValidator_ThrowsError_WhenToCountryCodeStringIsWrongLength(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToCountryCode);
        }

        [Theory]
        [InlineData("AE")]
        [InlineData("FS")]
        [InlineData("SU")]
        [InlineData("BC")]
        [InlineData("LZ")]
        [InlineData("SS")]
        public void ApiRequestValidator_ThrowsError_WhenUnrecognizedCountryEncountered(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToCountryCode);
        }

        [Theory]
        [InlineData("US")]
        [InlineData("CA")]
        [InlineData("AU")]
        [InlineData("IT")]
        [InlineData("BG")]
        [InlineData("HR")]
        [InlineData("SK")]
        public void ApiRequestValidator_NoError_WhenToCountryCodeStringIsKnownCountry(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.ToCountryCode);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_WhenToCityIsNull() {
            var model = new ApiCalculationRequest {
                                                      ToCity = null
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToCity);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_WhenToCityIsEmptyString() {
            var model = new ApiCalculationRequest {
                                                      ToCity = string.Empty
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToCity);
        }

        [Theory]
        [InlineData("city!")]
        [InlineData("411")]
        [InlineData("$@-.,")]
        [InlineData("<c17y>")]
        [InlineData("%$#@!&^*()")]
        public void ApiRequestValidator_ThrowsError_WhenCityStringIsNotAlphabetic(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCity = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToCity);
        }

        [Theory]
        [InlineData("city-city")]
        [InlineData("city, name")]
        [InlineData("St. Mary's St.")]
        [InlineData("\r\n\r\n\r\n  \t\t")]
        [InlineData("'.,- .,.'")]
        public void ApiRequestValidator_NoError_WhenCityStringHasValidLettersValue(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCity = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.ToCity);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_WhenStreetAddressIsNull() {
            var model = new ApiCalculationRequest {
                                                      ToStreetAddress = null
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToStreetAddress);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_WhenStreetAddressIsEmpty() {
            var model = new ApiCalculationRequest {
                                                      ToStreetAddress = string.Empty
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToStreetAddress);
        }

        [Fact]
        public void ApiRequestValidator_NoError_WhenStreetAddressHasContent() {
            var model = new ApiCalculationRequest {
                                                      ToStreetAddress = "123 Main St."
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.ToStreetAddress);
        }

        [Theory]
        [InlineData("AU")]
        [InlineData("GB")]
        [InlineData("IT")]
        [InlineData("SK")]
        [InlineData("FR")]
        [InlineData("HR")]
        public void ApiRequestValidator_NoError_WhenNonNorthAmericanCountryMissingToState(string input) {
            var model = new ApiCalculationRequest {
                                                      ToStateCode = null,
                                                      ToCountryCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.ToStateCode);
        }

        [Theory]
        [InlineData("CA")]
        [InlineData("US")]
        public void ApiRequestValidator_NoError_WhenNorthAmericanCountryHasToState(string input) {
            var model = new ApiCalculationRequest {
                                                      ToStateCode = _faker.Random.String2(2),
                                                      ToCountryCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.ToStateCode);
        }

        [Theory]
        [InlineData("IR")]
        [InlineData("BG")]
        [InlineData("GB")]
        [InlineData("AU")]
        [InlineData("CY")]
        [InlineData("DK")]
        public void ApiRequestValidator_NoError_WhenEuropeanCountryHasToStateSet(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = input,
                                                      ToStateCode = _faker.Random.String2(2)
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.ToStateCode);
        }

        [Theory]
        [InlineData("US")]
        [InlineData("CA")]
        public void ApiRequestValidator_ThrowsError_WhenAmericanCountryHasInvalidLengthToStateValue(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = input,
                                                      ToStateCode = _faker.Random.String2(_faker.Random.Int(3, 10))
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToStateCode);
        }

        [Theory]
        [InlineData("US")]
        [InlineData("CA")]
        public void ApiRequestValidator_ThrowsError_WhenAmericanCountryHasNumbersInToStateCode(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = input,
                                                      ToStateCode = _faker.Random.Int(10, 99).ToString()
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToStateCode);
        }

        [Theory]
        [InlineData("DE")]
        [InlineData("FI")]
        [InlineData("LT")]
        [InlineData("MT")]
        public void ApiRequestValidator_ThrowsError_WhenEuropeanCountryHasNumbersInToStateCode(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = input,
                                                      ToStateCode = _faker.Random.Int(10, 99).ToString()
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToStateCode);
        }

        [Theory]
        [InlineData("US")]
        [InlineData("CA")]
        [InlineData("GB")]
        [InlineData("HR")]
        public void ApiRequestValidator_ThrowsError_WhenToPostalCodeIsNull(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = input,
                                                      ToPostalCode = null
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToPostalCode);
        }

        [Theory]
        [InlineData("US")]
        [InlineData("CA")]
        [InlineData("GB")]
        [InlineData("HR")]
        public void ApiRequestValidator_ThrowsError_WhenToPostalCodeIsEmptyString(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = input,
                                                      ToPostalCode = string.Empty
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToPostalCode);
        }

        [Theory]
        [InlineData("ABCDE")]
        [InlineData("ABCDE-FGHI")]
        [InlineData("VIDEO-1234")]
        [InlineData("12345-ABCD")]
        [InlineData("123456789")]
        [InlineData("1234-12345")]
        [InlineData("---------")]
        public void ApiRequestValidator_ThrowsError_WhenAmericanToPostalCodeIsInvalid(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = CountryCodes.America,
                                                      ToPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToPostalCode);
        }

        [Theory]
        [InlineData("12345")]
        [InlineData("12345-1234")]
        [InlineData("98657")]
        [InlineData("30332-1004")]
        public void ApiRequestValidator_NoError_WhenAmericanToPostalCodeIsValid(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = CountryCodes.America,
                                                      ToPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.ToPostalCode);
        }

        [Theory]
        [InlineData("12345")]
        [InlineData("54321-1234")]
        [InlineData("ABCD")]
        [InlineData("FOOBAR")]
        [InlineData("lowercase")]
        [InlineData("$#!%^")]
        public void ApiRequestValidator_ThrowsError_WhenAustralianToPostalCodeIsInvalid(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = CountryCodes.Australia,
                                                      ToPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToPostalCode);
        }

        [Theory]
        [InlineData("1234")]
        [InlineData("5678")]
        [InlineData("0000")]
        [InlineData("9245")]
        public void ApiRequestValidator_NoError_WhenAustralianToPostalCodeSucceeds(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = CountryCodes.Australia,
                                                      ToPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.ToPostalCode);
        }

        [Theory]
        [InlineData("a1g 3f4")]
        [InlineData("a5z4b7")]
        [InlineData("G5F7X7")]
        [InlineData("123 456")]
        [InlineData("ABC 123")]
        [InlineData("4F5 D3T")]
        public void ApiRequestValidator_ThrowsError_WhenCanadianToPostalCodeInvalid(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = CountryCodes.Canada,
                                                      ToPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.ToPostalCode);
        }

        [Theory]
        [InlineData("D4H 4V7")]
        [InlineData("D8J 8G6")]
        [InlineData("K5J 5H2")]
        public void ApiRequestValidator_NoError_WhenCanadianToPostalCodeIsValid(string input) {
            var model = new ApiCalculationRequest {
                                                      ToCountryCode = CountryCodes.Canada,
                                                      ToPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.ToPostalCode);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_WhenFromCountryCodeIsNull() {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = null
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromCountryCode);
        }

        [Theory]
        [InlineData("")]
        [InlineData("a")]
        [InlineData("abc")]
        [InlineData("!3.")]
        [InlineData("floccinaucinihilipilification")]
        public void ApiRequestValidator_ThrowsError_WhenFromCountryCodeStringIsWrongLength(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromCountryCode);
        }

        [Theory]
        [InlineData("AE")]
        [InlineData("FS")]
        [InlineData("SU")]
        [InlineData("BC")]
        [InlineData("LZ")]
        [InlineData("SS")]
        public void ApiRequestValidator_ThrowsError_WhenUnrecognizedFromCountryEncountered(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromCountryCode);
        }

        [Theory]
        [InlineData("US")]
        [InlineData("CA")]
        [InlineData("AU")]
        [InlineData("IT")]
        [InlineData("BG")]
        [InlineData("HR")]
        [InlineData("SK")]
        public void ApiRequestValidator_NoError_WhenFromCountryCodeStringIsKnownCountry(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.FromCountryCode);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_WhenFromCityIsNull() {
            var model = new ApiCalculationRequest {
                                                      FromCity = null
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromCity);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_WhenFromCityIsEmptyString() {
            var model = new ApiCalculationRequest {
                                                      FromCity = string.Empty
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromCity);
        }

        [Theory]
        [InlineData("city!")]
        [InlineData("411")]
        [InlineData("$@-.,")]
        [InlineData("<c17y>")]
        [InlineData("%$#@!&^*()")]
        public void ApiRequestValidator_ThrowsError_WhenFromCityStringIsNotAlphabetic(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCity = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromCity);
        }

        [Theory]
        [InlineData("city-city")]
        [InlineData("city, name")]
        [InlineData("St. Mary's St.")]
        [InlineData("\r\n\r\n\r\n  \t\t")]
        [InlineData("'.,- .,.'")]
        public void ApiRequestValidator_NoError_WhenFromCityStringHasValidLettersValue(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCity = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.FromCity);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_WhenFromStreetAddressIsNull() {
            var model = new ApiCalculationRequest {
                                                      FromStreetAddress = null
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromStreetAddress);
        }

        [Fact]
        public void ApiRequestValidator_ThrowsError_WhenFromStreetAddressIsEmpty() {
            var model = new ApiCalculationRequest {
                                                      FromStreetAddress = string.Empty
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromStreetAddress);
        }

        [Fact]
        public void ApiRequestValidator_NoError_WhenFromStreetAddressHasContent() {
            var model = new ApiCalculationRequest {
                                                      FromStreetAddress = "123 Main St."
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.FromStreetAddress);
        }

        [Theory]
        [InlineData("AU")]
        [InlineData("GB")]
        [InlineData("IT")]
        [InlineData("SK")]
        [InlineData("FR")]
        [InlineData("HR")]
        public void ApiRequestValidator_NoError_WhenNonNorthAmericanCountryMissingFromState(string input) {
            var model = new ApiCalculationRequest {
                                                      FromStateCode = null,
                                                      FromCountryCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.FromStateCode);
        }

        [Theory]
        [InlineData("CA")]
        [InlineData("US")]
        public void ApiRequestValidator_NoError_WhenNorthAmericanCountryHasFromState(string input) {
            var model = new ApiCalculationRequest {
                                                      FromStateCode = _faker.Random.String2(2),
                                                      FromCountryCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.FromStateCode);
        }

        [Theory]
        [InlineData("IR")]
        [InlineData("BG")]
        [InlineData("GB")]
        [InlineData("AU")]
        [InlineData("CY")]
        [InlineData("DK")]
        public void ApiRequestValidator_NoError_WhenEuropeanCountryHasFromStateSet(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = input,
                                                      FromStateCode = _faker.Random.String2(2)
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.FromStateCode);
        }

        [Theory]
        [InlineData("US")]
        [InlineData("CA")]
        public void ApiRequestValidator_ThrowsError_WhenAmericanCountryHasInvalidLengthFromStateValue(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = input,
                                                      FromStateCode = _faker.Random.String2(_faker.Random.Int(3, 10))
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromStateCode);
        }

        [Theory]
        [InlineData("US")]
        [InlineData("CA")]
        public void ApiRequestValidator_ThrowsError_WhenAmericanCountryHasNumbersInFromStateCode(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = input,
                                                      FromStateCode = _faker.Random.Int(10, 99).ToString()
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromStateCode);
        }

        [Theory]
        [InlineData("DE")]
        [InlineData("FI")]
        [InlineData("LT")]
        [InlineData("MT")]
        public void ApiRequestValidator_ThrowsError_WhenEuropeanCountryHasNumbersInFromStateCode(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = input,
                                                      FromStateCode = _faker.Random.Int(10, 99).ToString()
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromStateCode);
        }

        [Theory]
        [InlineData("US")]
        [InlineData("CA")]
        [InlineData("GB")]
        [InlineData("HR")]
        public void ApiRequestValidator_ThrowsError_WhenFromPostalCodeIsNull(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = input,
                                                      FromPostalCode = null
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromPostalCode);
        }

        [Theory]
        [InlineData("US")]
        [InlineData("CA")]
        [InlineData("GB")]
        [InlineData("HR")]
        public void ApiRequestValidator_ThrowsError_WhenFromPostalCodeIsEmptyString(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = input,
                                                      FromPostalCode = string.Empty
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromPostalCode);
        }

        [Theory]
        [InlineData("ABCDE")]
        [InlineData("ABCDE-FGHI")]
        [InlineData("VIDEO-1234")]
        [InlineData("12345-ABCD")]
        [InlineData("123456789")]
        [InlineData("1234-12345")]
        [InlineData("---------")]
        public void ApiRequestValidator_ThrowsError_WhenAmericanFromPostalCodeIsInvalid(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = CountryCodes.America,
                                                      FromPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromPostalCode);
        }

        [Theory]
        [InlineData("12345")]
        [InlineData("12345-1234")]
        [InlineData("98657")]
        [InlineData("30332-1004")]
        public void ApiRequestValidator_NoError_WhenAmericanFromPostalCodeIsValid(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = CountryCodes.America,
                                                      FromPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.FromPostalCode);
        }

        [Theory]
        [InlineData("12345")]
        [InlineData("54321-1234")]
        [InlineData("ABCD")]
        [InlineData("FOOBAR")]
        [InlineData("lowercase")]
        [InlineData("$#!%^")]
        public void ApiRequestValidator_ThrowsError_WhenAustralianFromPostalCodeIsInvalid(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = CountryCodes.Australia,
                                                      FromPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromPostalCode);
        }

        [Theory]
        [InlineData("1234")]
        [InlineData("5678")]
        [InlineData("0000")]
        [InlineData("9245")]
        public void ApiRequestValidator_NoError_WhenAustralianFromPostalCodeSucceeds(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = CountryCodes.Australia,
                                                      FromPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.FromPostalCode);
        }

        [Theory]
        [InlineData("a1g 3f4")]
        [InlineData("a5z4b7")]
        [InlineData("G5F7X7")]
        [InlineData("123 456")]
        [InlineData("ABC 123")]
        [InlineData("4F5 D3T")]
        public void ApiRequestValidator_ThrowsError_WhenCanadianFromPostalCodeInvalid(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = CountryCodes.Canada,
                                                      FromPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.FromPostalCode);
        }

        [Theory]
        [InlineData("D4H 4V7")]
        [InlineData("D8J 8G6")]
        [InlineData("K5J 5H2")]
        public void ApiRequestValidator_NoError_WhenCanadianFromPostalCodeIsValid(string input) {
            var model = new ApiCalculationRequest {
                                                      FromCountryCode = CountryCodes.Canada,
                                                      FromPostalCode = input
                                                  };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.FromPostalCode);
        }
    }
}
