﻿using System;
using Bogus;
using FluentValidation;
using FluentValidation.TestHelper;
using TaxCalculator.Core.Errors;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;
using TaxCalculator.TaxJarApi.Validators.Request.Calculation;
using Xunit;

namespace TaxCalculator.TaxJarApi.Tests.Validators {
    public class LineItemValidatorTests {
        private readonly Faker _faker;

        private readonly LineItemValidator _sut;

        public LineItemValidatorTests() {
            _sut = new LineItemValidator();
            _faker = new Faker();
        }

        [Fact]
        public void LineItemValidator_ThrowsError_WhenIdIsNull() {
            var model = new LineItem {
                                         Id = null
                                     };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.Id);
        }

        [Fact]
        public void LineItemValidator_ThrowsError_WhenIdIsEmpty() {
            var model = new LineItem {
                                         Id = string.Empty
                                     };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.Id);
        }

        [Fact]
        public void LineItemValidator_NoError_WhenIdIsPresent() {
            var model = new LineItem {
                                         Id = _faker.Random.String()
                                     };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.Id);
        }

        [Fact]
        public void LineItemValidator_ThrowsError_WhenQuantityIsNegative() {
            var model = new LineItem {
                                         Quantity = _faker.Random.Int(max: -1)
                                     };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.Quantity);
        }

        [Fact]
        public void LineItemValidator_NoError_WhenQuantityIsPositive() {
            var model = new LineItem() {
                                           Quantity = _faker.Random.Int(1)
                                       };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.Quantity);
        }

        [Fact]
        public void LineItemValidator_ThrowsError_WhenDiscountIsNegative() {
            var model = new LineItem {
                                         NonUnitItemDiscount = _faker.Random.Decimal(decimal.MinValue, -0.001M)
                                     };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.NonUnitItemDiscount);
        }

        [Fact]
        public void LineItemValidator_ThrowsError_IfDiscountGreaterThanPrice() {
            var model = new LineItem {
                                         UnitPrice = _faker.Random.Decimal(0.01M, decimal.MaxValue - 1M),
                                     };
            model.NonUnitItemDiscount = _faker.Random.Decimal(model.UnitPrice, decimal.MaxValue);

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.NonUnitItemDiscount);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(40000)]
        public void LineItemValidator_NoError_IfDiscountIsZeroOrMore(decimal discount) {
            var model = new LineItem {
                                         NonUnitItemDiscount = discount,
                                         UnitPrice = discount + 1M
                                     };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.NonUnitItemDiscount);
        }

        [Fact]
        public void LineItemValidator_ThrowsError_WhenUnitPriceIsNegative() {
            var model = new LineItem {
                                         UnitPrice = _faker.Random.Decimal(max: -0.01M)
                                     };

            var result = _sut.TestValidate(model);
            result.ShouldHaveValidationErrorFor(x => x.UnitPrice);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(0.25)]
        [InlineData(1)]
        [InlineData(100000)]
        public void LineItemValidator_NoError_WhenPriceIsZeroOrGreater(decimal price) {
            var model = new LineItem {
                                         UnitPrice = price
                                     };

            var result = _sut.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(x => x.UnitPrice);
        }
    }
}
