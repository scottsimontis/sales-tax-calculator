﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

namespace TaxCalculator.TaxJarApi.Interfaces.RequestBuilders {
    public interface ICanadianRateRequestBuilder {
        public ICanadianRateRequestBuilder WithOriginCountryCode(string country);

        public ICanadianRateRequestBuilder WithOriginPostalCode(string postal);

        public ICanadianRateRequestBuilder WithOriginCity(string city);

        public ICanadianRateRequestBuilder WithOriginStateCode(string state);

        public ICanadianRateRequestBuilder WithOriginStreetAddress(string street);

        public ICanadianRateRequestBuilder WithDestinationCountryCode(string country);

        public ICanadianRateRequestBuilder WithDestinationPostalCode(string postal);

        public ICanadianRateRequestBuilder WithDestinationCity(string city);

        public ICanadianRateRequestBuilder WithDestinationStateCode(string state);

        public ICanadianRateRequestBuilder WithDestinationStreetAddress(string street);

        public ICanadianRateRequestBuilder WithOrderTotal(decimal amount);

        public ICanadianRateRequestBuilder WithShipping(decimal amount);

        public ICanadianRateRequestBuilder WithCustomerId(string id);

        public ICanadianRateRequestBuilder WithExemption(string exemption);

        public ICanadianRateRequestBuilder AddNexus(NexusAddress nexus);

        public ICanadianRateRequestBuilder AddLineItem(LineItem lineItem);

        public ICanadianRateRequestBuilder Reset();

        public ILineItemBuilder LineItemBuilder { get; }

        public INexusAddressBuilder NexusBuilder { get; }

        public Either<ErrorList, ApiCalculationRequest> Build();
    }
}
