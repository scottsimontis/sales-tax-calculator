﻿using System.Linq;
using FluentValidation;
using TaxCalculator.Core.Validation;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

namespace TaxCalculator.TaxJarApi.Validators.Request.Calculation {
    public class ApiRequestValidator : AbstractValidator<ApiCalculationRequest> {
        private readonly IValidator<LineItem> _lineItemValidator;

        private readonly IValidator<NexusAddress> _nexusValidator;

        public ApiRequestValidator(IValidator<LineItem> lineItemValidator,
            IValidator<NexusAddress> nexusValidator) {
            _lineItemValidator = lineItemValidator;
            _nexusValidator = nexusValidator;

            RuleFor(x => x)
                .MustHaveAmountPresent()
                .MustNotHaveBothAmountsSet();

            RuleFor(x => x.ToCountryCode)
                .MustHaveValue()
                .MustHaveExactLengthOf(2)
                .TaxJarMustSupportCountry();

            RuleFor(x => x.ToCity)
                .MustHaveValue()
                .MustBeComposedOfLetters();

            RuleFor(x => x.ToStreetAddress)
                .MustHaveValue();

            RuleFor(x => x.ToStateCode)
                .MustBeToStateIfNorthAmerican()
                .MustHaveLettersOnly();

            RuleFor(x => x.ToPostalCode)
                .MustBeValidPostalCodeToCountry();

            RuleFor(x => x.FromCountryCode)
                .MustHaveValue()
                .MustHaveExactLengthOf(2)
                .TaxJarMustSupportCountry();

            RuleFor(x => x.FromStateCode)
                .MustBeFromStateIfNorthAmerican()
                .MustHaveLettersOnly();

            RuleFor(x => x.FromCity)
                .MustHaveValue()
                .MustBeComposedOfLetters();

            RuleFor(x => x.FromStreetAddress)
                .MustHaveValue();

            RuleFor(x => x.FromPostalCode)
                .MustBeValidPostalCodeFromCountry();

            RuleFor(x => x.Amount)
                .AmountIsPositive()
                .When(x =>
                    x.LineItems == null || !x.LineItems.Any());

            RuleFor(x => x.TotalShipping)
                .ScalarNotNegative();

            RuleFor(x => x.LineItems)
                .CollectionEmptyOrNull()
                .When(x => x.Amount != default)
                .CollectionHasValues()
                .When(x => x.Amount == default);

            RuleForEach(x => x.LineItems)
                .SetValidator(_lineItemValidator);

            RuleForEach(x => x.NexusAddresses)
                .SetValidator(_nexusValidator);
        }
    }
}
