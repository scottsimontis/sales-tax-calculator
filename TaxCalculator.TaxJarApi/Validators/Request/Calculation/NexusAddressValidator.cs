﻿using FluentValidation;
using TaxCalculator.Core.Validation;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

namespace TaxCalculator.TaxJarApi.Validators.Request.Calculation {
    public class NexusAddressValidator : AbstractValidator<NexusAddress> {
        public NexusAddressValidator() {
            RuleFor(x => x.Id)
                .MustHaveValue();

            RuleFor(x => x.StateCode)
                .MustHaveValue()
                .MustHaveExactLengthOf(2);

            RuleFor(x => x.CountryCode)
                .MustHaveValue()
                .MustHaveExactLengthOf(2);
        }
    }
}
