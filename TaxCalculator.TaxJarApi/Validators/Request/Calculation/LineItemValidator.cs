﻿using FluentValidation;
using TaxCalculator.Core.Validation;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

namespace TaxCalculator.TaxJarApi.Validators.Request.Calculation {
    public class LineItemValidator : AbstractValidator<LineItem> {
        public LineItemValidator() {
            RuleFor(x => x.Id)
                .MustHaveValue();

            RuleFor(x => x.Quantity)
                .AmountIsPositive();

            RuleFor(x => x.NonUnitItemDiscount)
                .ScalarNotNegative()
                .LineItemDiscountCannotExceedPrice();

            RuleFor(x => x.UnitPrice)
                .ScalarNotNegative();
        }
    }
}
