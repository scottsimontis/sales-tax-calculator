﻿using System.Text.Json.Serialization;

namespace TaxCalculator.TaxJarApi.Models.Responses.Rates {
    public class EuropeanTaxRateResponse {
        [JsonPropertyName("name")]
        public string CountryName { get; private set; }

        [JsonPropertyName("standard_rate")]
        public string StandardRate { get; private set; }

        [JsonPropertyName("reduced_rate")]
        public string ReducedRate { get; private set; }

        [JsonPropertyName("super_reduced_rate")]
        public string SuperReducedRate { get; private set; }

        [JsonPropertyName("parking_rate")]
        public string ParkingRate { get; set; }

        [JsonPropertyName("distance_sales_threshold")]
        public string DistanceSalesThreshold { get; private set; }
    }
}
