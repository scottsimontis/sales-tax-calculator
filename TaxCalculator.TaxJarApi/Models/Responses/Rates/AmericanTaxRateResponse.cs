﻿using System.Text.Json.Serialization;

namespace TaxCalculator.TaxJarApi.Models.Responses.Rates {
    public class AmericanTaxRateResponse : ApiTaxRatesResponse {
        [JsonPropertyName("country_rate")]
        public string CountryRate { get; private set; }

        public string State { get; private set; }

        [JsonPropertyName("state_rate")]
        public string StateRate { get; private set; }

        public string County { get; private set; }

        [JsonPropertyName("county_rate")]
        public string CountyRate { get; private set; }

        public string City { get; private set; }

        [JsonPropertyName("city_rate")]
        public string CityRate { get; private set; }

        /// <summary>
        /// Aggregate of city and county sales tax districts relevant to location
        /// </summary>
        [JsonPropertyName("combined_district_rate")]
        public string CombinedDistrictRate { get; private set; }

        /// <summary>
        /// Aggregate of city, district, county, and state rates relevant to location.
        /// This value should be used when performing manual rate calculations instead
        /// of using the Calculation end point also supplied in this API.
        /// </summary>
        [JsonPropertyName("combined_rate")]
        public string CombinedRate { get; private set; }
    }
}
