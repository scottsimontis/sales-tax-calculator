﻿using System.Text.Json.Serialization;

namespace TaxCalculator.TaxJarApi.Models.Responses.Rates {
    public abstract class ApiTaxRatesResponse {
        [JsonPropertyName("zip")]
        public string PostalCode { get; protected set; }

        [JsonPropertyName("country")]
        public string CountryIsoCode { get; protected set; }

        [JsonPropertyName("combined_rate")]
        public string CombinedRate { get; protected set; }

        [JsonPropertyName("freight_taxable")]
        public bool FreightTaxable { get; protected set; }
    }
}
