﻿using System.Text.Json.Serialization;

namespace TaxCalculator.TaxJarApi.Models.Requests.Calculation {
    public class LineItem {
        public string Id { get; internal set; }

        public int Quantity { get; internal set; }

        [JsonPropertyName("product_tax_code")]
        public string ProductTaxCode { get; internal set; }

        [JsonPropertyName("unit_price")]
        public decimal UnitPrice { get; internal set; }

        [JsonPropertyName("discount")]
        public decimal NonUnitItemDiscount { get; internal set; }

}
}
