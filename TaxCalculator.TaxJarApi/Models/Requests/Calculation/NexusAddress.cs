﻿using System.Text.Json.Serialization;

namespace TaxCalculator.TaxJarApi.Models.Requests.Calculation {
    public class NexusAddress {
        public string Id { get; internal set; }

        [JsonPropertyName("country")]
        public string CountryCode { get; internal set; }

        [JsonPropertyName("zip")]
        public string PostalCode { get; internal set; }

        [JsonPropertyName("state")]
        public string StateCode { get; internal set; }

        public string City { get; internal set; }

        [JsonPropertyName("street")]
        public string StreetAddress { get; internal set; }
    }
}
