﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TaxCalculator.TaxJarApi.Models.Requests.Calculation {
    public class ApiCalculationRequest {
        [JsonPropertyName("from_country")]
        public string FromCountryCode { get; internal set; }

        [JsonPropertyName("from_zip")]
        public string FromPostalCode { get; internal set; }

        [JsonPropertyName("from_state")]
        public string FromStateCode { get; internal set; }

        [JsonPropertyName("from_city")]
        public string FromCity { get; internal set; }

        [JsonPropertyName("from_street")]
        public string FromStreetAddress { get; internal set; }

        [JsonPropertyName("to_country")]
        public string ToCountryCode { get; internal set; }

        [JsonPropertyName("to_zip")]
        public string ToPostalCode { get; internal set; }

        [JsonPropertyName("to_state")]
        public string ToStateCode { get; internal set; }

        [JsonPropertyName("to_city")]
        public string ToCity { get; internal set; }

        [JsonPropertyName("to_street")]
        public string ToStreetAddress { get; internal set; }

        public decimal Amount { get; internal set; }

        [JsonPropertyName("shipping")]
        public decimal TotalShipping { get; internal set; }

        [JsonPropertyName("customer_id")]
        public string CustomerId { get; internal set; }

        [JsonPropertyName("exemption_type")]
        public string ExemptionType { get; internal set; }

        internal List<NexusAddress> NexusLocations;

        internal List<LineItem> LineItems;

        [JsonPropertyName("nexus_addresses")] public IEnumerable<NexusAddress> NexusAddresses => NexusLocations;

        [JsonPropertyName("line_items")] public IEnumerable<LineItem> LineItemCollection => LineItems;

        public ApiCalculationRequest() {
            NexusLocations = new List<NexusAddress>();
            LineItems = new List<LineItem>();
        }
    }
}
