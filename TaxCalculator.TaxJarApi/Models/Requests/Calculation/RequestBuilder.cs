﻿using TaxCalculator.Core.Validation;
using TaxCalculator.TaxJarApi.Interfaces.RequestBuilders;

namespace TaxCalculator.TaxJarApi.Models.Requests.Calculation {
    public class RequestBuilder : IRateRequestBuilder {
        private readonly IAmericanRateRequestBuilder _americanRate;

        private readonly IAustralianRateRequestBuilder _australianRate;

        private readonly ICanadianRateRequestBuilder _canadianRate;

        private readonly IEuropeanRateRequestBuilder _europeanRate;

        public IAmericanRateRequestBuilder ForAmerica() => _americanRate;

        public IAustralianRateRequestBuilder ForAustralia() => _australianRate;

        public ICanadianRateRequestBuilder ForCanada() => _canadianRate;

        public IEuropeanRateRequestBuilder ForEurope() => _europeanRate;

        public RequestBuilder(IAmericanRateRequestBuilder america,
            IAustralianRateRequestBuilder australia,
            ICanadianRateRequestBuilder canada,
            IEuropeanRateRequestBuilder europe) {
            _americanRate = america.ThrowIfNull(nameof(australia));
            _australianRate = australia.ThrowIfNull(nameof(australia));
            _canadianRate = canada.ThrowIfNull(nameof(canada));
            _europeanRate = europe.ThrowIfNull(nameof(europe));
        }
    }
}
