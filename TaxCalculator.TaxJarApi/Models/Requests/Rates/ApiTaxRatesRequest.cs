﻿using System.Text.Json.Serialization;

namespace TaxCalculator.TaxJarApi.Models.Requests.Rates {
    public class ApiTaxRatesRequest {
        public string Country { get; private set; }

        [JsonPropertyName("zip")]
        public string PostalCode { get; private set; }

        public string State { get; private set; }

        public string City { get; private set; }

        [JsonPropertyName("street")]
        public string StreetAddress { get; private set; }
    }
}
